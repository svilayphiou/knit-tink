# KNIT-TINK

**Projet de recherche artistique de Stéphanie Vilayphiou, 2021–2022.**



## De *Live Textile* à *knit-tink*

Le projet initial *Live Textile* a légèrement changé de direction. Le projet initial proposait d’expérimenter l’aspect *live* de la fabrication textile commandée par ordinateur via des machines open source ou hackées. Les complications techniques ont reporté cette idée à un projet ultérieur. Mais l’essence du projet, qui est d’explorer le lien entre textile et numérique, et bel et bien présente et se retrouve donc dans l’intitulé «knit-tink». Dans ce dossier sont regroupées toutes les recherches et expérimentations que j’ai traversées.

La recherche peut se décomposer en trois parties qui parfois s’entrelacent :

- des tapisseries de femmes ayant un rôle important dans l’histoire de l’informatique;
- des motifs générés par du code informatique;
- la construction d’un métier à tisser jacquard.

## My Mother Was a Computer

My Mother is a Computer est une série de portraits de femmes ayant un rôle important dans l’histoire du numérique afin de rappeler que l’informatique n’est pas uniquement une histoire d’hommes comme le laisse entendre les proportions de femmes dans les milieux du numérique. Non seulement elles sont présentes mais elles ont contribué de manière non-négligeable à ce que sont les langages de programmation aujourd’hui. La tapisserie prend ici un rôle symbolique, il m’était important que ces textiles ne soient pas fonctionnels.

Ce travail a été initié avant l’octroi de la subvention Arts Numériques avec les portraits d’Ada Lovelace et Grace Hopper. La série a été poursuivie par les portraits de Vera Molnár et de Margaret Hamilton ainsi qu’avec le titre de la série empruntée au titre du livre de Katherine N. Hayles, théoricienne des médias. 

**Ada Lovelace** a été la première personne à concevoir l’idée de logiciel informatique en 1843 en imaginant les potentialités de la Machine Analytique de Charles Babbage. 
**Grace Hopper** a grandement contribué à ce que sont les langages de programmation modernes. Elle est à l’origine entre autres des concepts de librairies logicielles, de la documentation, du code en langage naturel.
**Margaret Hamilton** a mis en place la notion de priorités dans un code: lorsque la machine est saturée en calculs, elle s’occupe d’abord des tâches prioritaires. Ceci a permis l’atterrissage de la mission Apollo 11 sur la Lune en 1969. 
**Vera Molnár** est une artiste peintre française d’origine hongroise qui explore des compositions algorithmiques avant même d’avoir accès à un ordinateur. Ensuite, elle utilise l’ordinateur pour créer des compositions qu’elle reproduit ensuite en peinture.

Travail exposé pendant le Parcours d’Artistes de Saint-Gilles et Forest 2021. Le portrait de Margaret Hamilton a été tricoté collectivement par le public après une conférence que j’ai donnée sur le lien entre textile et informatique pour célébrer la journée Ada Lovelace 2021.

 ![](tricot/183514013_1381044855562436_8678622126318294714_n.jpg) ![](tricot/185217585_493026251969451_4548942569420131491_n.jpg)

​		↑↑↑ Ada Lovelace ↑↑↑				 ↑↑↑ Grace Hopper ↑↑↑


![20211010_145143](tricot/20211010_145143.jpg)

​				↑↑↑ Titre de la série: My Mother Was a Computer ↑↑↑

![](tricot/20211006_172731.jpg) ![20211009_180645](tricot/20211009_180645.jpg)
		↑↑↑ Vera Molnár ↑↑↑			 	↑↑↑ Margaret Hamilton ↑↑↑

 


## Electric eel wheel

Le subside m’a permis d’acheter directement auprès du producteur le rouet électrique Electric Eel Wheel 6.0 ou eew6. Le rouet m’a permise d’avancer beaucoup plus rapidement mon apprentissage du filage et de fabriquer les fils qui auront servis dans mes expérimentations textiles. Ce rouet est un projet open source — les plans sont disponibles sur le web — et son concepteur reste dans cette philosophie-ci: il demande l’avis de ses clients quant à la l’amélioration ou production de nouveaux produits. Il augmente les potentialités du eew6 en concevant des accessoires que chacun·e peut imprimer en 3D dans un fablab local.



### Premiers essais

![](filage/20210527_151732.jpg) ![20210529_093438_01](filage/20210529_093438_01.jpg)

![20210529_094431](filage/20210529_094431.jpg) ![20210529_094949](filage/20210529_094949.jpg)

 

 

![20210531_141739](filage/20210531_141739.jpg) ![20210531_142609](filage/20210531_142609.jpg)

![20210606_173707](filage/20210606_173707.jpg) ![20210606_173747](filage/20210606_173747.jpg)



### Filage de fils noirs et blancs

Pour renforcer le lien entre textile et code, je tenais à travailler en noir et blanc. Pour cela, j’ai fabriqué moi-même des fils avec des toisons de moutons destinées à être jetées. J’utilise des teintes naturelles de moutons blancs et de moutons noirs.

![20220711_173853](filage/20220711_173853.jpg) ![20220712_153204](filage/20220712_153204.jpg)

​	↑↑↑ Lavage de la laine ↑↑↑				↑↑↑ Cardage de la laine ↑↑↑	

​			

 ![20220808_104034](filage/20220808_104034.jpg) ![20220808_111655](filage/20220808_111655.jpg) ![20220808_145500](filage/20220808_145500.jpg) ![20220808_113445](filage/20220808_113445.jpg)


![Sample Video](filage/20210602_163318.mp4)



### Bobinoir DIY

Pour pouvoir tisser, on prépare une certaine longueur de fils sur des navettes. Ici, l’écartement des fils n’est pas très large donc j’utilise plutôt des flûtes sans navette. Pour préparer le fil sur les flûtes, j’ai testé deux solutions qui marchent très bien:

- accrocher la flûte dans une perceuse.
- imprimer en 3D une bobine pour le rouet électrique eew6.


![Sample Video](filage/20220810_132040.mp4)  ![Sample Video](filage/299813991_165846709326053_3932491110652004156_n.mp4)




## Ada Dietz

Ada Dietz était enseignante de mathématiques et biologie ainsi que tisserande. Dans les années 50, elle publie **Algebraic Expressions in Handwoven Textiles*, une méthode nouvelle pour générer des motifs de tissage basés sur des motifs algébriques.

Expérimentations de motifs avec un outil en ligne qui offre une interface graphique aux algorithmes d’Ada Dietz. Les deux motifs générés représentent les extrêmes des  paramètres disponibles afin de comprendre ce sur quoi ils influent. Les motifs ont ensuite été tricotés sur une machine à tricoter hackée.

<<https://demonstrations.wolfram.com/AdaDeitzPolynomialsForHandwovenTextiles/>>



![](https://demonstrations.wolfram.com/AdaDeitzPolynomialsForHandwovenTextiles/img/popup_1.png)

​						↑↑↑ Capture d’écran du logiciel ↑↑↑

![](tricot/20210902_144735.jpg) ![20210902_173132](tricot/20210902_173132.jpg)

​	↑↑↑ Ada Dietz. 4 2 2 2 ↑↑↑		 	↑↑↑ Ada Dietz. 2 26 2 1 ↑↑↑


![Sample Video](tricot/20210902_163306_1_1_1.mp4)




## Python Itertools

Expérimentations avec la librarie *itertools* du langage de programmation Python. Cette librairie permet de manipuler des listes (*arrays*). Ceci permet d’avoir un motif et de pouvoir en faire des variations suite à des procédés logiques. Dans l’exemple ci-dessous, on retrouve des permutations, produit, combinaisons, combinaisons avec remplacement.

Le script Python me permet de générer des motifs ASCII. J'utilise alors le format d'image `.xpm` pour pouvoir ouvrir cette image dans un logiciel de traitement d'images, Gimp en l'occurrence.

![20220623_104354](itertools/20220623_104354.jpg) ![20220623_104332](itertools/20220623_104332.jpg) 
		↑↑↑ Programme Python ↑↑↑		↑↑↑ Résultats dans un Terminal ↑↑↑

 ![](itertools/20220623_103819.jpg) ![20220623_104018](itertools/20220623_104018.jpg)

​						↑↑↑ Résultats ouverts dans Gimp ↑↑↑

 ![20220823_164122](tissage sur machine à tricoter/20220823_164122.jpg) ![20220823_164057](tissage sur machine à tricoter/20220823_164057.jpg) 
 					↑↑↑ Tricot machine recto/verso ↑↑↑



Script Python:
```
import itertools 

repetition = 3
hauteur = 50

def itertostr(liste):
    return "".join([''.join(h) for h in [list(k) for k in liste]])

### Commenter/décommenter l'une des quatre lignes ci-dessous pour choisir l'une ou l'autre des manipulations 
#pattern_orig = itertools.permutations(".||.", repetition)
#pattern_orig = itertools.combinations(".||.", repetition)
#pattern_orig = itertools.combinations_with_replacement(".||.", repetition)
pattern_orig = itertools.product(".||.", repeat=repetition)


pattern = itertostr(pattern_orig)

ligne = pattern
ligne2 = "".join(list(reversed(pattern)))

header = """! XPM2
%d %s 2 1
| c #000000
. c #ffffff
""" % (len(pattern), hauteur)

ligne += "\n"
ligne2 += "\n"

print(header+(ligne+ligne2)*hauteur)
```

![]()



## ADAcad

Expérimentations avec le logiciel AdaCAD, élaboré par Laura Devendorf d’[Unstable Design Lab](https://unstable.design/). AdaCAD est un environnement de programmation nodal qui permet de générer des motifs spécifiquement pour le tissage.

<<https://adacad.org>>

![](../adacad/2022-05-31-100658_grim.png) ![20220608_170415](adacad/20220608_170415.jpg)
					↑↑↑ Capture d’écran du logiciel ↑↑↑



<img src="../adacad/blocs-selvedge.jpg" alt="blocs-selvedge" style="zoom:200%;" /> <img src="../adacad/blocs-rayures.jpg" alt="blocs-rayures" style="zoom:200%;" /> <img src="../adacad/blocs-selvedge_white-tabby.jpg" alt="blocs-selvedge_white-tabby" style="zoom:200%;" />
​					↑↑↑ Variations autour de blocs ↑↑↑



![20220815_214604](tissage sur machine à tricoter/20220815_214604.jpg)![20220815_215209](tissage sur machine à tricoter/20220815_215209.jpg)

​					↑↑↑ Motif généré tricoté en machine ↑↑↑



## Apprendre à tisser

L’idée de base de ma recherche étant d’expérimenter avec un métier à tisser jacquard DIY, j’ai d’abord voulu m’exercer quelque peu à des techniques de tissage plus conventionnelles. Le tissage est selon moi la technique textile la plus simple et la plus complexe à la fois. Le point de toile, qui est le plus basique, peut être réalisé par un enfant de 4 ans. Mais lorsqu’on veut sortir des armures classiques, les possibilités sont infinies par rapport à d’autres techniques comme le tricot ou le crochet.




![20210924_181759](apprentissage tissage/20210924_181759.jpg) ![20211001_091658](apprentissage tissage/20211001_091658.jpg)

​		↑↑↑ Montage d’une chaîne sur un métier à tisser à cadres ↑↑↑

![20220601_161822](apprentissage tissage/20220601_161822.jpg)![20220607_230923](apprentissage tissage/20220607_230923.jpg)

​				↑↑↑ Tests de différentes armures/structures ↑↑↑

![20220601_161752](apprentissage tissage/20220601_161752.jpg) ![20220601_161800](apprentissage tissage/20220601_161800.jpg)

​			↑↑↑ Comprendre comment faire un motif bicolore ↑↑↑

 

## Liveloom

Dans la proposition de départ de ce subside était la construction du métier à tisser LiveLoom conçu par l’artiste et développeur Alex McLean. Après des complications techniques et ayant trouvé une autre manière de fabriquer un métier à tisser jacquard, j’ai laissé de côté cette piste. Mais voici tout de même une documentation de mon processus pour les personnes qui souhaitent construire ce métier open source.

 

![](liveloom/20210627_213029.jpg) ![20210627_215610](liveloom/20210627_215610.jpg)

↑↑↑ Découpe laser multiplex 3mm ↑↑↑		↑↑↑ Assemblage des pieds ↑↑↑

![20210627_220711](liveloom/20210627_220711.jpg) 

↑↑↑ Connexion des pieds avec la plaque. Maintien des pièces parallélépipédiques avec des élastiques. ↑↑↑

![20210627_223949](liveloom/20210627_223949.jpg) ![20210627_224301](liveloom/20210627_224301.jpg) 

​				↑↑↑ Assemblage du support des solenoïdes ↑↑↑

![20210627_224551](liveloom/20210627_224551.jpg)

​			↑↑↑ Connexion du support des solénoïdes à la base ↑↑↑

![20210627_230137](liveloom/20210627_230137.jpg) ![20210627_230142](liveloom/20210627_230142.jpg)

↑↑↑ Mise en place sur une pique à barbecue des languettes de bois. Celles-ci seront poussées par les solenoïdes. Ici, elles ont été montées à l’envers. ↑↑↑

# PHOTO LANGUETTE À L’ENDROIT

![20210628_133957](liveloom/20210628_133957.jpg) ![20210628_155400](liveloom/20210628_155400.jpg)

​	↑↑↑ Premiers tests peu fructueux par manque de tension dans les fils ↑↑↑

![20210823_161049](liveloom/20210823_161049.jpg) ![20210810_112753](liveloom/20210810_112753.jpg)

↑↑↑ Utilisation de poids de pêche pour maintenir la tension des fils de chaîne ↑↑↑

​						![20210823_161027](liveloom/20210823_161027.jpg) 

​				↑↑↑ Les pièces en multiplex sont assez fragiles… ↑↑↑

![20220112_125645](liveloom/20220112_125645.jpg)

​						↑↑↑ Préparation de l’électronique ↑↑↑

![20220112_131151](liveloom/20220112_131151.jpg) ![20220113_103358](liveloom/20220113_103358.jpg)

​				↑↑↑ Gauche: placement incorrect des transistors. 
​	Droite: Les parties noires des transistors doivent être vers le centre. ↑↑↑

![20220113_103405](liveloom/20220113_103405.jpg) ![20220113_103422](liveloom/20220113_103422.jpg)

 			↑↑↑ Le sens de branchement est très important. 
 	Ici, Alex McLean m’avait envoyé une nappe déja utilisée et donc annotée. ↑↑↑

![20220113_103446](liveloom/20220113_103446.jpg)

​	↑↑↑ J’ai coupé un câble d’alimentation de PC pour les insérer dans les slots. ↑↑↑

# PHOTO ALIMENTATION

 ![20220323_090418](liveloom/20220323_090418.jpg) ![20220528_123115](liveloom/20220528_123115.jpg)

​	↑↑↑ N’ayant pas de rondelles, j’en ai découpé au laser dans du feutre. ↑↑↑

Une fois le métier à tisser monté, j’ai eu beaucoup de problèmes au niveau logiciel. Le Liveloom est connecté par un Arduino à l’ordinateur, et celui-ci est contrôlé via le logiciel Tidal Cycles, créé par Alex McLean. Ce logiciel est extrêmement compliqué à installer. Après une session de déboguage avec Alex McLean, j’ai réussi à installer Tidal Cycles, et à faire bouger des solenoïdes. Mais la position des solenoïdes ne correspondaient pas aux commandes que j’envoyais. Ayant une autre piste de métier à tisser jacquard, j’ai donc laissé celle-ci de côté… Aussi parce que le Liveloom est limité à 16 fils en largeur.

 

## Transformer une Machine à tricoter en métier à tisser Jacquard

Le designer textile Kurt Payne a détourné une machine à tricoter à cartes perforées pour faire du tissage.  Au lieu de mettre du fil dans le chariot de la machine, un fil est placé sur chaque aiguille pour construire la chaîne du métier à tisser. La carte perforée permet de sélectionner des aiguilles pour créer un motif et fait donc monter les fils de chaîne pour pouvoir passer la navette avec le fil de trame.

Je me suis donc inspirée de ce processus pour avoir un métier à tisser jacquard commandé par un ordinateur. 



#### TIssage à la ceinture + Poids de pêche

#### + Sélection des aiguilles à la main



Il existe différents systèmes de métier à tisser. Ceux qu’on voit en grande majorité en Europe sont des métiers à tisser à cadres posés sur le sol ou sur une table où la chaîne est horizontale.

Mais traditionnellement, il existe d’autres types de métiers à tisser dont :

- des métiers à tisser verticaux: les fils de chaîne sont à la verticale et sont tendus grâce à des poids. Le métier à tisser LiveLoom s’en inspire.

- des métiers à tisser à ceinture: les fils de chaîne sont fixés à un objet statique (un arbre) et au corps de la personne qui tisse à l’aide d’une ceinture.

Pour un premier prototype, j’ai mélangé ces deux types de métiers à tisser pour pouvoir transformer aisément la machine à tricoter en métier à tisser. 



Pour bien tester le dispositif mécanique avant d’y brancher un ordinateur, j’ai décidé de sélectionner les aiguilles à la main pour composer des motifs simples. Chaîne blanche en laine.

![20220817_114452](tissage sur machine à tricoter/20220817_114452.jpg) ![20220817_115035](tissage sur machine à tricoter/20220817_115035.jpg) 

↑↑↑ Les poids sont enfilés sur une boucle de fil afin d’être placés plus facilement ↑↑↑



![20220523_180903](tissage sur machine à tricoter/20220523_180903.jpg) ![20220624_091921](tissage sur machine à tricoter/20220624_091921.jpg)


​						↑↑↑ Tissage à la ceinture ↑↑↑


![Sample Video](tissage sur machine à tricoter/20220624_090928.mp4)






#### Sélection automatique des aiguilles



Une fois le dispositif testé, j’ai utilisé le logiciel Ayab pour envoyer le motif préparé sur AdaCAD à la machine à tricoter. J’ai testé la technique de tissage *overshot* et jacquard pour voir les différences. Le tissage overshot permet d’avoir un tissage plus stable car le fil de trame blanc est un point de toile qui permet de stabiliser le motif. Dans le jacquard, les flottés trop longs amènent une déformation.

Chaîne blanche en coton.
Trame blanche coton, trame noir coton.


![20220706_203443](tissage sur machine à tricoter/20220706_203443.jpg) ![20220706_203458](tissage sur machine à tricoter/20220706_203458.jpg) 

​	↑↑↑ tissage *overshot*, trame en coton blanc et laine noire filée main ↑↑↑

#### Ajout d'un peigne de tissage



Après consultation de la tisserande Amandine Brun Sauvant, j’ai introduit un peigne de tissage dans le dispositif. Le premier, en impression 3D, était beaucoup trop imprécis. Le second prototype est en plexiglas découpé au laser. Le peigne ralentit la cadence de tissage, probablement parce que le plastique n’est pas assez lisse. Il a aussi tendance à ronger le fil de chaîne, ici en laine filée main, il y aurait probablement moins de souci si la chaîne avait été en coton.

Chaîne blanche en laine filée main.
Trame noire en laine filée main et en coton. 

![20220817_114316](tissage sur machine à tricoter/20220817_114316.jpg) ![20220817_114408](tissage sur machine à tricoter/20220817_114408.jpg)

↑↑↑ Préparation des fils de chaîne ↑↑↑    ↑↑↑ Accrochage du peigne et enfilage ↑↑↑

 ![20220817_114653](tissage sur machine à tricoter/20220817_114653.jpg) ![20220817_114749](tissage sur machine à tricoter/20220817_114749.jpg) 





![20220812_162819](tissage sur machine à tricoter/20220812_162819.jpg)

​	↑↑↑ en haut: tissage jacquard — trame noire et blanche filées main; en bas: tissage overshot — trame blanche coton, trame noire en laine filée main ↑↑↑


![Sample Video](tissage sur machine à tricoter/20220706_153409_1.mp4)





#### Fonction ImageMap dans AdaCAD



Le motif de test généré sur AdaCAD est assez simple pour être tissé sur un métier à tisser à cadres avec une simple combinaison de pédales. Pour vraiment tester les capacités du dispositif, j’ai tissé l’image d’exemple du logiciel AdaCAD, en deux couleurs dans un premier temps.  

Chaîne blanche en laine filée main.
Trame noire en laine filée main.
Trame blanche en laine filée main et en coton.

![20220812_162824](tissage sur machine à tricoter/20220812_162824.jpg) 

![20220812_172559](tissage sur machine à tricoter/20220812_172559.jpg) ![20220812_173423](tissage sur machine à tricoter/20220812_173423.jpg) 

![Sample Video](tissage sur machine à tricoter/20220812_164557.mp4)









##### Ada Lovelace



Une fois le premier motif de test réalisé, j'ai voulu tisser le portrait d'Ada Lovelace. Un tissage type overshot s’est avéré indispensable car sans ça, l’image était tassée verticalement et donc illisible.

Chaîne blanche en laine filée main.
Trame noire en laine filée main.
Trame blanche en coton.

![20220817_180040](tissage sur machine à tricoter/20220817_180040.jpg)

​			↑↑↑ 1er test *overshot*: échec total car des flottés trop grands. ↑↑↑


 ![20220818_182148](tissage sur machine à tricoter/20220818_182148.jpg) ![20220818_183111](tissage sur machine à tricoter/20220818_183111.jpg)

​				↑↑↑ Si on ne met pas l’image en négatif dans Ayab, 
​			lors du tissage, nous voyons l’envers (photo de droite) ↑↑↑

#### ![20220819_171632](tissage sur machine à tricoter/20220819_171632.jpg)

​	↑↑↑ Comparaison du portrait d’Ada en tissage (gauche) et en tricot (droite) ↑↑↑



#### Remplacement de la ceinture par une ensouple

#### + Tissage en plusieurs couleurs



Le système d’accroche à l’avant au corps était très fatigant et très lent. C’est un système utile pour tester rapidement le dispositif ou pour tricoter un ruban, mais dès qu’on veut augmenter les échelles, il devient trop encombrant. Une pseudo-ensouple a été mise à l’avant de la machine à tricoter en utilisant un porte-rouleau à papier Ikea trouvé dans l’atelier accroché avec des serre-joints sur une table roulante. Je pensais utiliser des roulettes pour pouvoir éloigner la table (et donc tirer les fils) au fur et à mesure du tissage, mais finalement, il est préférable de garder la table fixe pour deux raisons: garder le dispositif bien parallèle; pouvoir accéder au chariot de la machine à tricoter soit pour le pousser, soit pour maintenir le chariot dans le moteur pour des grandes largeurs de tissage. 

Chaîne blanche en laine filée main.
Trame blanche, rose, bleue en coton mercerisé.

![20220819_160632](tissage sur machine à tricoter/20220819_160632.jpg) ![20220819_160640](tissage sur machine à tricoter/20220819_160640.jpg)

↑↑↑ Système d’ensouple à l’avant ↑↑↑    		↑↑↑ Motif trop tassé ↑↑↑

![20220819_171720](tissage sur machine à tricoter/20220819_171720.jpg) ![20220819_171731](tissage sur machine à tricoter/20220819_171731.jpg)

​						↑↑↑ tissage overshot recto/verso ↑↑↑


![Sample Video](tissage sur machine à tricoter/20220819_161247.mp4)





#### Remplacement des poids de pêche par une Barre 

#### + Tissage sans peigne



Le peigne ralentissait beaucoup la cadence de tissage. Le test sans peigne permet d’aller très vite car la navette en bois fait office de sabre à tisser: il sert à ouvrir la chaîne et à battre les fils de trame. Bien que cela ait bien fonctionné au début du tissage, au fur et à mesure, le tissage s’élargissait et les fils de chaîne devenaient beaucoup trop espacés. Cela a pour conséquence d’avoir un tissage non homogène et trop lâche à la fin.

J’ai voulu tester de remplacer les poids de pêche par une barre en métal ou ici un peigne en métal pour voir si cela aidait à avoir des longueurs de fils homogènes et un bon système de tension.

![20220824_110352](tissage sur machine à tricoter/20220824_110352.jpg) ![20220824_110356](tissage sur machine à tricoter/20220824_110356.jpg)

​    ↑↑↑ Accrochage du peigne juste en-dessous des aiguilles à tricoter ↑↑↑

 ![20220824_110430](tissage sur machine à tricoter/20220824_110430.jpg) ![20220824_110910](tissage sur machine à tricoter/20220824_110910.jpg) 

​	↑↑↑ Fil plié en deux accroché sur une dent de peigne et sur les aiguilles ↑↑↑

![20220824_125938](tissage sur machine à tricoter/20220824_125938.jpg) ![20220824_130022](tissage sur machine à tricoter/20220824_130022.jpg) 

​						↑↑↑ Accrochage à l’ensouple ↑↑↑

![20220824_130948](tissage sur machine à tricoter/20220824_130948.jpg) ![20220824_131010](tissage sur machine à tricoter/20220824_131010.jpg) 

​						↑↑↑ Pré-nouage des fils ↑↑↑

![20220824_131014](tissage sur machine à tricoter/20220824_131014.jpg) ![20220824_131032](tissage sur machine à tricoter/20220824_131032.jpg) 

​			↑↑↑ Descente du peigne-poids pour tendre les fils ↑↑↑

![20220824_131038](tissage sur machine à tricoter/20220824_131038.jpg) ![20220824_131135](tissage sur machine à tricoter/20220824_131135.jpg) 

​			↑↑↑ Vérification que tous les fils soient bien tendus ↑↑↑

![20220824_131256](tissage sur machine à tricoter/20220824_131256.jpg) ![20220824_131317](tissage sur machine à tricoter/20220824_131317.jpg) 

​	↑↑↑ Resserrage des fils et nouage. Ici, les fils devraient passer d’abord
par l’extérieur pour revenir vers l’intérieur pour un nœud plus proche des fils ↑↑↑



#### Motifs Itertools



Tissage des motifs générés avec Python Itertools que j’avais déjà tricoté avec la machine hackée.

![20220915_113559](tissage sur machine à tricoter/20220915_113559.jpg)

![Sample Video](tissage sur machine à tricoter/20220826_110708.mp4)

![Sample Video](tissage sur machine à tricoter/20220826_123415.mp4)





#### Ajout d’un peigne de tissage en métal 



La conclusion des deux derniers tests m’a amenée à utiliser un vrai peigne de tissage en métal pour permettre aux fils de chaîne de bien glisser. Cela permet aussi d’utiliser des peignes avec une densité plus dense qu’avec un peigne DIY.

![20220826_153906](tissage sur machine à tricoter/20220826_153906.jpg) ![20220826_153911](tissage sur machine à tricoter/20220826_153911.jpg)

#### armures jacquard



Après des tests jacquard peu satisfaisants, et afin de mieux comprendre comment le tissage jacquard et AdaCAD fonctionnent, j’ai préparé des armures pour les assigner à des zones de couleurs. Des tests sur des primitives géométriques formant un diagramme de Venn où je cherchais à créer entre les deux formes un mélange des couleurs. Un autre test avec le portrait de Grace Hopper qui est à l’origine très contrasté donc idéal pour avoir de grandes plages de couleurs et leur assigner des armures fort distinctes.

Chaîne en coton blanc (100 fils).
Trames bleue et rose en coton mercerisé.



![](tissage sur machine à tricoter/2022-09-15-140913_grim.png)

​							↑↑↑ Capture d’écran AdaCAD ↑↑↑



​				![](../adacad/grace-seuil.png) ![20220902_152421](tissage sur machine à tricoter/20220902_152421.jpg)

​			↑↑↑ Couleurs techniques importées dans AdaCAD (gauche); 
​					tissage de Grace Hopper (droite) ↑↑↑



![](tissage sur machine à tricoter/20220908_122631.jpg) ![](tissage sur machine à tricoter/20220908_122648.jpg)

​			↑↑↑ Grace Hopper et diagramme de Venn (recto/verso) ↑↑↑

![](tissage sur machine à tricoter/20220908_122637.jpg) ![](tissage sur machine à tricoter/20220908_122653.jpg)

​				↑↑↑ Diagrammes de Venn (recto/verso) ↑↑↑



#### Montage de 200 fils de chaîne



Maintenant que le métier fonctionne plutôt bien, j’ai voulu tester le maximum de fils de chaîne que me permet la machine à tricoter, à savoir 200 aiguilles/fils. Amandine Brun Sauvant m’a conseillé d’ourdir la chaîne pour avoir une longueur plus importante. Mais il s’est avéré qu’utiliser 200 fils de chaîne formait un angle trop important entre la largeur du tissage et la largeur de la machine à tricoter; il était par conséquent impossible de bouger le peigne sans que les fils de chaîne perdent leur tension. J’ai donc dû réduire à 150 fils de chaîne. Il serait possible d’utiliser les 200 fils si la table était plus éloignée de la machine à tricoter.


![Sample Video](tissage sur machine à tricoter/20220908_132124.mp4)

​				↑↑↑ Ourdissage de 200 fils de 3m de long ↑↑↑



![](tissage sur machine à tricoter/20220909_095435.jpg) ![](tissage sur machine à tricoter/20220912_090500.jpg)

​						↑↑↑ Montage de la chaîne ↑↑↑



![](tissage sur machine à tricoter/20220912_135638.jpg)

​		↑↑↑ Estelle a rajouté des vis sur la tige en bois du porte-rouleau 
​			afin de pouvoir enrouler le tissage et le maintenir en place ↑↑↑



![](tissage sur machine à tricoter/20220912_155333.jpg)

​						↑↑↑ Des caisses ont été utilisées pour lester le rouleau de fils de chaîne en-dessous de la machine à tricoter, mais c’est un système peu commode à remettre en place à chaque fois qu’on veut enrouler le tissage. ↑↑↑



![](tissage sur machine à tricoter/20220915_145227.jpg)

​			↑↑↑ J’ai finalement suspendu le rouleau par des fils à la table de la machine à tricoter et ajouté des poids pour maintenir une tension verticale ↑↑↑






#### Tests d’armures



La tisserande Estelle Saignes m’a aidée à développer des armures sur le logiciel AdaCAD. Une fois ces armures échantillonnées, on peut plus aisément choisir quelle armure va représenter quelle couleur ou niveau de gris dans une image qu’on souhaite reproduire.

<img src="tissage sur machine à tricoter/2022-09-15-144624_grim.png" style="max-width:80%;" />

​		↑↑↑ AdaCAD: armures poches réalisées par Estelle Saignes ↑↑↑



![](tissage sur machine à tricoter/20220913_153859.jpg)![](tissage sur machine à tricoter/20220913_162224.jpg) 

​	↑↑↑ Tests de différentes armures (gauche: sergés; droite: poches) ↑↑↑



#### Portrait D’Ada Dietz 

#### + armures aléatoires



Pour compléter ma série My Mother was a Computer, mon dernier test a été de tisser une photo d’Ada Dietz sur son métier à tisser. Ada Dietz étant tisserande, il me paraissait plus juste d’utiliser le tissage plutôt que le tricot.. J’ai voulu expérimenter avec une fonctionnalité d’AdaCAD que je trouve assez singulière. Il est possible de générer des armures aléatoires mais tout en ayant un contrôle sur le pourcentage de trame qu’on laisse apparaître; c’est-à-dire le pourcentage de pixels noirs et blancs. J’ai donc exploité cette fonctionnalité en prenant une image avec 10 niveaux de gris. À chaque niveau de gris est alors associé une armure aléatoire dont le pourcentage de trame apparente correspond au pourcentage de noir dans la teinte.



![2022-09-15-140744_grim](tissage sur machine à tricoter/2022-09-15-140744_grim.png)

​				↑↑↑ AdaCAD: armures aléatoires selon niveau de gris ↑↑↑



​						![](tissage sur machine à tricoter/20220914_171323.jpg) 

<img src="tissage sur machine à tricoter/20220914_171233.jpg" style="max-width:80%;" />

​				↑↑↑ Portrait d’Ada Dietz sur son métier à tisser ↑↑↑

![Sample Video](tissage sur machine à tricoter/20220914_165032.mp4)





#### Conclusions techniques



L’utilisation de la ceinture est une bonne manière de prototyper rapidement mais lorsqu’on veut commencer à expérimenter plus longuement voire penser à de la production, mieux vaut ajouter une ensouple. Le corps est ainsi beaucoup moins sollicité. Le tissage est plus stable et reste parallèle à la machine à tricoter.

Malgré qu’il soit plus rapide de tisser sans peigne, celui-ci est bel et bien important pour contrôler la densité et la largeur du tissage. Car plus on avance dans le tissage, plus la densité de la chaîne faiblit. Si on tisse un ruban étroit, on pourrait passer outre.

L’utilisation de poids de pêche permet un montage de la chaîne très rapide et sûr mais on est limité à la hauteur entre le sol et la machine à tricoter.

Le dispositif mérite encore quelques améliorations notables: ajouter une ensouple à l’avant et une ensouple en-dessous de la machine à tricoter afin d’avoir une tension optimale des fils et permettre une longueur de tissage intéressante. J’ai en tête un système simple à fabriquer à l’aide d’une découpeuse laser et une imprimante 3D.

Les designers textile qui m’ont accompagnées, Amandine Brun Sauvant et Estelle Saignes, trouvent intéressant d’avoir un tel métier à tisser pour pouvoir prototyper rapidement. Dans un processus plus classique, on demanderait à une entreprise de réaliser des prototypes qui sont renvoyés par courrier ou alors il faut réserver un métier à tisser jacquard dans un fablab textile mais les places sont très rares. Quant à l’aspect logiciel, Amandine et Estelle trouvent AdaCAD très performant, avec une approche paramétrique intéressante. Le logiciel Poincarre utilisé dans les formations de design textile (dont la licence coûte environ 13000€ et non disponible sous Linux) permet sans doute énormément de fonctionnalités mais AdaCAD permet déjà de transformer son image en armures de tissage, de faire des couches de tissage multiples et propose une approche algorithmique qui est nouvelle par rapport à des logiciels de tissage existants.



## Suites potentielles



Il y aurait encore énormément de pistes à explorer. Parmi celles que je souhaiterai poursuivre dans un deuxième temps:

- explorer des fils de chaînes de couleurs différentes: noir et blanc mais aussi CMJN + blanc pour pouvoir faire des corrélations avec une imprimante offset. En tant qu’ancienne designer graphique, je souhaite explorer des procédés de tramage CMJN transposés au tissage. 
- explorer plus encore des motifs générés par programmation (Python, AdaCAD), notamment après la lecture de cette thèse découverte en fin de recherche: [*Glitching the Fabric: Strategies of new media art applied to the codes of knitting and weaving*](https://gupea.ub.gu.se/handle/2077/57324?locale-attribute=sv) de David N.G. McCallum.
- explorer le côté *live* de la machine, de pouvoir interagir plus directement avec l’image à tricoter/tisser notamment en s’inspirant du projet de [Lining Yao](https://dl.acm.org/doi/fullHtml/10.1145/3411764.3445750).



Tout ce travail de recherche sera exposé pendant Brussels Design September 2022 à Green Fabric.





Écrit le 15 septembre 2022.







​										![](http://www.artsplastiques.cfwb.be/fileadmin/common/cfwb_css/img/page_footer_fwb.jpg)
​				Avec le soutien de la Commission Arts Numériques 
​						de la Fédération Wallonie—Bruxelles.



## Licence

Toutes les productions de ce dépôt (sauf mention contraire) sont en licence [Art Libre](https://artlibre.org/) et [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/be/). Pour toute réutilisation, merci de citer la maternité de l’œuvre au nom de Stéphanie Vilayphiou. Tous les fichiers sont disponibles sur <<https://gitlab.com/svilayphiou/knit-tink>.


