# knit-tink

Ce dépôt Git constitue la recherche de Stéphanie Vilayphiou entre le textile et le code, plus particulièrement de construire un métier à tisser jacquard à commande numérique.

La recherche peut se décomposer en trois parties qui parfois s’entrelacent :

- des tapisseries de femmes ayant un rôle important dans l’histoire de l’informatique;
- des motifs générés par du code informatique;
- la construction d’un métier à tisser jacquard.

Cette page résumé les «résultats» du projet. Les résultats sont de statuts variables, certains sont des objets finis, d’autres relèvent encore de l’expérimentation mais n’en sont pas nécessairement moins intéressants pour autant.
La [page de documentation](doc/README.md) relate de manière plus détaillée le processus de recherche dans un ordre chronologique.

## Résultats

### My Mother Was a Computer

 ![](doc/tricot/183514013_1381044855562436_8678622126318294714_n.jpg) ![](doc/tricot/185217585_493026251969451_4548942569420131491_n.jpg)

​	↑↑↑ Premiers portraits pré-subside: Ada Lovelace et Grace Hopper ↑↑↑


![](doc/tricot/20211010_145143.jpg)

​				↑↑↑ Titre de la série: My Mother Was a Computer ↑↑↑

![](doc/tricot/20211006_172731.jpg) ![20211009_180645](doc/tricot/20211009_180645.jpg)
		↑↑↑ Vera Molnár ↑↑↑			 	↑↑↑ Margaret Hamilton ↑↑↑
		



### Algorithmic patterns

![](doc/tricot/20210902_144735.jpg) ![20210902_173132](doc/tricot/20210902_173132.jpg)

​	↑↑↑ Ada Dietz. 4 2 2 2 ↑↑↑		 	↑↑↑ Ada Dietz. 2 26 2 1 ↑↑↑



![20220823_164122](doc/tissage sur machine à tricoter/20220823_164122.jpg) ![20220823_164057](doc/tissage sur machine à tricoter/20220823_164057.jpg) 
 			↑↑↑ Motifs générés en Python tricotés machine ↑↑↑



![20220815_214604](doc/tissage sur machine à tricoter/20220815_214604.jpg)![20220815_215209](doc/tissage sur machine à tricoter/20220815_215209.jpg)

​					↑↑↑ Motif AdaCAD tricoté en machine ↑↑↑



### Tissage Jacquard

![20220819_171632](doc/tissage sur machine à tricoter/20220819_171632.jpg) ![20220902_152421](doc/tissage sur machine à tricoter/20220902_152421.jpg)

​		↑↑↑ Ada Lovelace ↑↑↑					↑↑↑ Grace Hopper ↑↑↑



![20220914_171323](doc/tissage sur machine à tricoter/20220914_171323.jpg)

​									↑↑↑ Ada Dietz ↑↑↑





## Dispositifs

Plusieurs dispositifs ont été utilisés pendant cette recherche. Le tout est documenté pour que chacun·e puisse réitérer l’expérience dans son propre atelier. Parmi ces dispositifs:

- un rouet électrique
- une machine à tricoter hackée
- le métier à tisser open source Liveloom d’Alex McLean
- une machine à tricoter hackée transformée en métier à tisser
 
 ![Sample Video](doc/filage/20210602_163318.mp4)

​						↑↑↑ rouet électrique eew6 ↑↑↑

 ![Sample Video](doc/tricot/20210902_163306_1_1_1.mp4)


​						↑↑↑ Machine à tricoter hackée ↑↑↑



![20220528_123115](doc/liveloom/20220528_123115.jpg)

​									↑↑↑ Liveloom ↑↑↑


![20220914_171233](doc/tissage sur machine à tricoter/20220914_171233.jpg)

​				↑↑↑ Métier à tisser jacquard sur machine à tricoter ↑↑↑



## Remerciements

Merci à [Alex McLean](https://yaxu.org/) pour son accompagnement technique sur le LiveLoom et les discussions autour des [motifs algorithmiques](https://algorithmicpattern.org/).
Merci à [Amandine Brun Sauvant](https://www.instagram.com/atelier.brunsauvant/) et [Estelle Saignes](https://www.instagram.com/estellesaignes/) pour leur expertise en tissage.
Merci à [Kurt Payne](https://www.instagram.com/manknitea/) pour l'inspiration ingénieuse d'utiliser une machine à tricoter comme métier à tisser jacquard.
Merci à [Unstable Design Lab](https://unstable.design/) pour le génialissime logiciel [AdaCAD](https://adacad.org).
Merci à [Ayab](https://ayab-knitting.com/) pour rendre les machines à tricoter hackables.
Merci à [Green Fabric](https://greenfabric.be) pour un environnement de travail axé sur le partage.
Merci à Julien Leresteux du [Fablab’ke](https://fablabke.be/) pour la découpe laser des pièces du Liveloom.
Merci à Ali Ray pour son soutien précieux et ses conseils judicieux.

​										![](http://www.artsplastiques.cfwb.be/fileadmin/common/cfwb_css/img/page_footer_fwb.jpg)
​				Avec le soutien de la Commission Arts Numériques 
​						de la Fédération Wallonie—Bruxelles.



## Licence

Toutes les productions de ce dépôt (sauf mention contraire) sont en licence [Art Libre](https://artlibre.org/) et [CC-BY-SA](https://creativecommons.org/licenses/by-sa/2.0/be/). Pour toute réutilisation, merci de citer la maternité de l’œuvre au nom de Stéphanie Vilayphiou. Tous les fichiers sont disponibles sur <<https://gitlab.com/svilayphiou/knit-tink>>.





Écrit le 15 septembre 2022.
