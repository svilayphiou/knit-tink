import itertools 

repetition = 2
hauteur = 50

def itertostr(liste):
    return "".join([''.join(h) for h in [list(k) for k in liste]])

pattern_orig = itertools.permutations("|.|.", repetition)
pattern_orig2 = itertools.combinations("|.|.", repetition)
pattern_orig3 = itertools.combinations_with_replacement("|.|.", repetition)
pattern_orig4 = itertools.product("|.|.", repeat=repetition)

pattern = itertostr(pattern_orig)
pattern2 = itertostr(pattern_orig2)*2
pattern3 = itertostr(pattern_orig3)
pattern4 = itertostr(pattern_orig4)



ligne = pattern
#ligne2 = "".join(list(reversed(pattern)))

header = """! XPM2
%d %s 2 1
| c #000000
. c #ffffff
""" % (len(pattern), hauteur)

pattern += "\n"
pattern2 += "\n"
pattern3 += "\n"
#pattern4 += "\n"

block = pattern+ pattern2+ pattern3+ pattern4
block2 = "".join(list(reversed(block)))

print(header+(block + block2)*hauteur)
