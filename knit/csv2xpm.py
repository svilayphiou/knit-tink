import sys

i = sys.argv[1]

f = open(i, "r")
lines = f.readlines()
f.close()

bbb = ""

for line in lines:
    l = line.split(";")
    for nb in l:
        try: 
            mod = int(nb) % 2
            if mod == 0:
                bbb += "V"
            else:
                bbb += "A"
        except: 
            pass
    bbb += "\n"
print(bbb)
header = """! XPM2
%d %s 2 1
A c #000000
V c #ffffff
""" % (len(lines[0])/2, len(lines))

o = i.split(".")[0] + ".xpm"
f = open(o, "w")
f.write(header)
f.write(bbb)
f.close()
