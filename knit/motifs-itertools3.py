import itertools 

repetition = 3
hauteur = 50

def itertostr(liste):
    return "".join([''.join(h) for h in [list(k) for k in liste]])

#pattern_orig = itertools.permutations(".||.", repetition)
#pattern_orig = itertools.combinations(".||.", repetition)
#pattern_orig = itertools.combinations_with_replacement(".||.", repetition)
pattern_orig = itertools.product(".||.", repeat=repetition)

pattern = itertostr(pattern_orig)



ligne = pattern
ligne2 = "".join(list(reversed(pattern)))

header = """! XPM2
%d %s 2 1
| c #000000
. c #ffffff
""" % (len(pattern), hauteur)

ligne += "\n"
ligne2 += "\n"

print(header+(ligne+ligne2)*hauteur)
